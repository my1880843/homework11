package Task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<Set<Integer>> setOfSet = new HashSet<>();
        setOfSet.add(new HashSet<>(List.of(1, 2, 3)));
        setOfSet.add(new HashSet<>(List.of(1, 3, 4)));
        setOfSet.add(new HashSet<>(List.of(1, 23, 32)));

        System.out.println(toFlatSet(setOfSet));
    }

    public static Set<Integer> toFlatSet(Set<Set<Integer>> set) {
        return new HashSet<>(set.stream().flatMap(Collection::stream).toList());
    }
}
