package Task5;

import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        printUpper(List.of("aaaaa", "bbfbfbf", "dfsfdf"));
    }

    public static void printUpper(List<String> input) {
        String str = input.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));

        System.out.println(str);
    }
}
