package Task4;

import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Double> list = List.of(1.0, 2.3, 2.0, 0.5);
        System.out.println(getOrderedByDesc(list));
    }

    public static List<Double> getOrderedByDesc(List<Double> input) {
        return input.stream().sorted(Comparator.reverseOrder()).toList();
    }
}
