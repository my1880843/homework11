package Task7;

import java.util.function.BiPredicate;

public class Main {
    public static void main(String[] args) {
        System.out.println(checkWords("cat", "cats"));
        System.out.println(checkWords("cat", "cut"));
        System.out.println(checkWords("cat", "nut"));
    }

    public static boolean checkWords(String input1, String input2) {
        BiPredicate<String, String> checkWords = (String word1, String word2)
                -> isEqualsAddOrDelete(word1, word2) || isEqualsUpdate(word1, word2);

        return checkWords.test(input1, input2);
    }

    private static boolean isEqualsUpdate(String word1, String word2) {
        int missMatchIndex = getMissMatchIndex(word1, word2);
        String word1WithoutMiss = new StringBuilder(word1).deleteCharAt(missMatchIndex).toString();
        String word2WithoutMiss = new StringBuilder(word2).deleteCharAt(missMatchIndex).toString();

        return word1.length() == word2.length()
                && word1WithoutMiss.equals(word2WithoutMiss);
    }

    private static boolean isEqualsAddOrDelete(String word1, String word2) {
        String longWord = word1.length() > word2.length() ? word1 : word2;
        String shortWord = word1.length() > word2.length() ? word2 : word1;

        int missMatchIndex = getMissMatchIndex(longWord, shortWord);
        String longWordWithoutMiss = new StringBuilder(longWord).deleteCharAt(missMatchIndex).toString();

        return longWordWithoutMiss.equals(shortWord);
    }

    private static int getMissMatchIndex(String longWord, String shortWord) {
        for (int i = 0; i < shortWord.length(); i++) {
            if (longWord.charAt(i) != shortWord.charAt(i))
                return i;
        }
        return shortWord.length();
    }
}
