package Task1;

import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        System.out.println(sumFromOneToHundred());
    }

    public static int sumFromOneToHundred() {
        return IntStream.range(1, 101).filter(x -> x % 2 == 0).sum();
    }
}
