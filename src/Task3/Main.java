package Task3;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(notEmptyStringCount(List.of("", " ", "1123", "123", " ")));
    }

    public static int notEmptyStringCount(List<String> input) {
        return (int) input.stream().filter(x -> !x.isEmpty()).count();
    }
}
