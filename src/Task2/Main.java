package Task2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println(multiplyOfInt(List.of(1, 2, 3, 4, 5)));
    }

    public static int multiplyOfInt(List<Integer> input) {
        return input.stream().reduce(1, (x, y) -> x * y);
    }
}
